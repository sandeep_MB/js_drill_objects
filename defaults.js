function defaults(obj, defaultProps) {
    for (key1 in obj) {
        for (key2 in defaultProps) {
            if (JSON.stringify(key1) === JSON.stringify(key2)) {
                obj[key1] = defaultProps[key2];
            }
        }
    }
    return obj;
}

module.exports = defaults;