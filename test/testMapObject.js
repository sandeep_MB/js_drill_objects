const obj = require('../object.js');
const mapObject = require('../mapObject.js');

const cb = (value) => {
    if (typeof value == 'string') {
        return value += ': String';
    }
    return value * 2
};

mapObject(obj, cb);

const actualObject = obj;
const expectedObject = { name: 'Bruce Wayne: String', age: 72, location: 'Gotham: String' };

if (JSON.stringify(expectedObject) === JSON.stringify(actualObject)) {
    console.log(actualObject);
} else {
    console.log('Objects are different');
}
