const obj = require('../object.js');
const invert = require('../invert.js');

const actualResult = invert(obj);
const expectedResult = { '36': 'age', 'Bruce Wayne': 'name', Gotham: 'location' };

if (JSON.stringify(actualResult) === JSON.stringify(expectedResult)) {
    console.log(actualResult);
} else {
    console.log('Actual result is different from expected Result');
}
