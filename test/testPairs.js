const obj = require('../object.js');
const pairs = require('../pairs.js');

const actualResult = pairs(obj);
const expectedResult = [ [ 'name', 'Bruce Wayne' ], [ 'age', 36 ], [ 'location', 'Gotham' ] ];

if (JSON.stringify(actualResult) === JSON.stringify(expectedResult)) {
    console.log(actualResult);
} else {
    console.log('Actual result is different from expected Result');
}
