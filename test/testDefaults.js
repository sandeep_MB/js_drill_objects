const obj = require('../object.js');
const _default = require('../defaults.js');

const defaultValue = { location: 'Central City' };

const actualResult = _default(obj, defaultValue);
const expectedResult = { name: 'Bruce Wayne', age: 36, location: 'Central City' };

if (JSON.stringify(actualResult) === JSON.stringify(expectedResult)) {
    console.log(actualResult);
} else {
    console.log('Actual result is different from expected Result');
}
