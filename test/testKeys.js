const keys = require('../keys.js');
const object = require('../object.js');

const actualResult = keys(object);
const expectedResult = ['name', 'age', 'location'];

if (JSON.stringify(actualResult) === JSON.stringify(expectedResult)) {
    console.log(actualResult);
} else {
    console.log('Actual result is different from expected Result');
}

