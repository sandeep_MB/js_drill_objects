function mapObject(obj, cb) {
    for (const key in obj) {
        if (obj.hasOwnProperty(key)) {
            obj[key] = cb(obj[key]);
        }
    }
}
module.exports = mapObject;