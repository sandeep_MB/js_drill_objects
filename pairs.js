function pairs(obj) {
  const array = [];
  for (let key in obj) {
      // result.push(`${key} : ${obj[key]}`)
    array.push([key, obj[key]]);
  }
  return array;
}

module.exports = pairs;