const findKeys = require('./keys.js')

function invert(obj) {
    const result = {};
    let keys = findKeys(obj);
    for (let index = 0; index < keys.length; index++) {
        result[obj[keys[index]]] = keys[index];
    }
    return result;
}

module.exports = invert;