function keys(obj) {
    const result = [];
    for (let key in obj) {
        result.push(`${key}`);
    }
    return result;
}

module.exports = keys;